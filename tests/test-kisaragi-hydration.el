(require 'buttercup)
(require 'kisaragi-hydration)

(describe "Utils"
  (it "returns list of dates between two dates"
    (expect
     (kisaragi-hydration--dates-between "2020-07-15" "2020-07-31")
     :to-equal
     '("2020-07-31" "2020-07-30" "2020-07-29" "2020-07-28" "2020-07-27"
       "2020-07-26" "2020-07-25" "2020-07-24" "2020-07-23" "2020-07-22"
       "2020-07-21" "2020-07-20" "2020-07-19" "2020-07-18" "2020-07-17"
       "2020-07-16" "2020-07-15")))
  (it "parses ISO dates well enough"
    (expect
     ;; Both of these use the current timezone on the system.
     (kisaragi-hydration--parse-date "2020-02-25")
     :to-equal
     (encode-time 0 0 0 25 2 2020))
    (expect
     (kisaragi-hydration--parse-date "2018-08-32")
     :to-equal
     (encode-time 0 0 0 32 8 2018))))

(describe "Visualizers"
  (it "displays whether target is passed"
    (expect
     (kisaragi-hydration--target-visualizer 150 20)
     :to-equal "✓")))
